provider "aws" {
  region = "us-east-1"
  profile = "dev"
}

resource "aws_instance" "DevOpsTest" {
  ami           = "ami-0c293f3f676ec4f90"
  instance_type = "t3.medium"
  count = 1
  vpc_security_group_ids = [
    "sg-0d7d4da3f6508f094"
  ]
  user_data = <<EOF
#!/bin/bash
cd /home/ec2-user/
sudo yum update
sudo yum install python37
python3 -m venv /path/to/new/virtual/environment > /etc/hostname
sudo yum search docker
sudo yum info docker
sudo yum install docker
EOF
  subnet_id = "subnet-0ea65567a53f058f6"
}
